<?php return [
    'plugin' => [
        'name' => 'MauticDotOrg Extensions',
        'description' => 'Plugin extensions to support Mautic.org',
    ],
    'status' => [
        'points' => 'User Points',
        'tab' => 'Mautician Status',
    ],
    'votable' => [
        'votable' => 'Votable',
        'votable_description' => 'Allows handlers for arbitrarily up/downvoting items'
    ],
    'editableBlogPost' => [
        'component' => [
            'name' => 'Editable Blog Post',
            'description' => 'Provides handler for frontend users to save contributed blog posts'
        ]
    ]
];